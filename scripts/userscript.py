import requests
from requests.auth import AuthBase
from pyknx import logger

API_KEY = '	c1722ccdef6f32c280d0bac6b9eb381150a2f8fd'
API_URL = 'http://127.0.0.1:8000/api/'


class TokenAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = 'Token ' + self.token
        return r


def save_to_db(context):
    data = {'search': context.object.id}
    r = requests.get(API_URL + 'objects/', params=data, auth=TokenAuth(API_KEY))
    if r.status_code != 200:
        logger.reportError("Getting object ID - Status code: {0}, Text: {1}".format(r.status_code, r.text))
        return 0

    results = r.json()['results']
    if len(results) != 0:
        object_id = results[0]['id']

        payload = {'obj': object_id, 'value': context.object.value}
        r = requests.post(API_URL + 'data/', data=payload, auth=TokenAuth(API_KEY))

        if r.status_code != 201:
            logger.reportError(
                "Saving data for obj id:{2} - Status code: {0}, Text: {1}".format(r.status_code, r.text, object_id))
            return 0
    else:
        payload = {'obj_id': context.object.id, 'name': context.object.caption, 'type': context.object.type, 'monitor': True}
        r = requests.post(API_URL + 'objects/', data=payload, auth=TokenAuth(API_KEY))

        object_id = r.json()['id']
        payload = {'obj': object_id, 'value': context.object.value}
        r = requests.post(API_URL + 'data/', data=payload, auth=TokenAuth(API_KEY))

        if r.status_code != 201:
            logger.reportError(
                "Saving data for obj id:{2} - Status code: {0}, Text: {1}".format(r.status_code, r.text, object_id))
            return 0
    return 1
