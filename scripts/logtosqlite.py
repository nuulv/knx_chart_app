from glob import glob
from datetime import datetime
import sqlite3

mypath = "C:\\Users\\Unknown\\Downloads\\linknx_data\\log\\"

conn = sqlite3.connect(mypath + 'db.sqlite3')

files = glob(mypath + "*.log")

for file in files:
    fn = file.split('\\')
    n = fn[-1]
    oin = n.split('.')
    obj_id_name = oin[0]

    c = conn.cursor()
    c.execute("INSERT INTO knxapp_knxobjects(obj_id, name, type, monitor) VALUES(?, ?, ?, ?)", (obj_id_name, n, 'unknown', True))
    obj_id = c.lastrowid
    conn.commit()
    print("{1}:{0} added to knxobjects".format(obj_id_name, obj_id))

    with open(file) as f:
        lines = f.readlines()
        list_to_insert = []

        for line in lines:
            #prepare data
            data = line.split()

            d = datetime.strptime(data[0] + " " + data[1], "%Y-%m-%d %H:%M:%S")

            if data[3] == 'on' or data[3] == 'off':
                v = True if data[3] == 'on' else False
            else:
                v = float(data[3])

            list_to_insert.append( (obj_id, d, v) )

        c.executemany("INSERT INTO knxapp_knxdata (obj_id, date, value) VALUES (?, ?, ?)", list_to_insert)
        conn.commit()
        print("Inserted {0} to database.".format(n))

conn.close()
