from jsonfield import JSONField
from django.db import models


class KnxObjects(models.Model):
    obj_id = models.CharField(max_length=50)
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=50)
    monitor = models.BooleanField(default=False)

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name_plural = "  Objekty knx"


class KnxData(models.Model):
    obj = models.ForeignKey(KnxObjects, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    value = JSONField()

    def __str__(self):
        return '%s | %s' % (self.obj, self.date)

    class Meta:
        verbose_name_plural = " Dane"
        ordering = ('date',)


class FavChart(models.Model):
    obj_ids = models.ManyToManyField(KnxObjects)

    start_date = models.DateTimeField(null=True)
    stop_date = models.DateTimeField(null=True)

    name = models.CharField(max_length=200)
    in_menu = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Zapisane wykresy"
        ordering = ('in_menu', 'name', )



'''
class KnxOptions(dbsettings.Group):
    hostname = dbsettings.StringValue(default='127.0.0.1', help_text='Nazwa hosta na którym pracuje serwer linknx')
    port = dbsettings.PositiveIntegerValue(default=1028, help_text='Port serwera linknx')


class MoreOptions(dbsettings.Group):
    test = dbsettings.StringValue('testing')


options = MoreOptions()
email = KnxOptions()
'''