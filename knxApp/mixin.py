from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import ContextMixin
from .models import *


class UserChartsMixin(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super(UserChartsMixin, self).get_context_data(**kwargs)
        context.update({
            'user_charts': FavChart.objects.filter(in_menu=True),
        });
        return context


class StandardMixin(UserChartsMixin, LoginRequiredMixin):
    pass