from rest_framework import serializers
from .models import KnxObjects, KnxData, FavChart


class KnxDataAllSerializer(serializers.ModelSerializer):
    """ Display all data of model """
    value = serializers.JSONField()

    class Meta:
        model = KnxData
        fields = ('value', 'date', )


class KnxDataForObjectsSerializer(serializers.ModelSerializer):
    """ Display only date and value """
    value = serializers.JSONField()

    class Meta:
        model = KnxData
        fields = ('date', 'value')


class KnxObjectsSerializer(serializers.ModelSerializer):
    """ Standard KnxObjects representation """
    class Meta:
        model = KnxObjects
        fields = '__all__'


class KnxObjectsWithDataSerializer(serializers.ModelSerializer):
    """ KnxObjects with data """
    knxdata_set = KnxDataForObjectsSerializer(many=True, read_only=True)

    class Meta:
        model = KnxObjects
        fields = '__all__'


class FavChartSerializer(serializers.ModelSerializer):
    """ Standard FavCharts representation """
    #obj_ids = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    obj_ids = serializers.PrimaryKeyRelatedField(many=True, queryset=KnxObjects.objects.all())

    class Meta:
        model = FavChart
        fields = '__all__'


# TODO optimize for large datasets like here:
# https://github.com/highcharts/highcharts/blob/master/samples/data/from-sql.php
class FavChartWithDataSerializer(serializers.ModelSerializer):
    """ FavCharts with data """
    obj_ids = KnxObjectsWithDataSerializer(many=True, read_only=True)

    class Meta:
        model = FavChart
        fields = '__all__'
