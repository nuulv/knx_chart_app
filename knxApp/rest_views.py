from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework.pagination import PageNumberPagination
from rest_framework import filters
import django_filters
from . import models
from . import serializers


class MultiSerializerViewSet(viewsets.ModelViewSet):
    serializers = {
        'default': None,
    }

    def get_serializer_class(self):
            return self.serializers.get(self.action, self.serializers['default'])


class CustomPageNumberPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'per_page'


class NoPagination(PageNumberPagination):
    page_size = None


class KnxDataFilter(django_filters.rest_framework.FilterSet):

    date_s = django_filters.DateTimeFilter(name="date", lookup_expr='gte')
    date_e = django_filters.DateTimeFilter(name="date", lookup_expr='lte')

    class Meta:
        model = models.KnxData
        fields = ['obj', 'date_s', 'date_e']


class KnxObjectsViewSet(MultiSerializerViewSet):
    pagination_class = CustomPageNumberPagination
    queryset = models.KnxObjects.objects.all()
    serializers = {
        'default': serializers.KnxObjectsSerializer,
        'dataset': serializers.KnxObjectsWithDataSerializer,
    }
    filter_backends = (filters.SearchFilter,)
    search_fields = ('obj_id', 'name', 'type', )

    @detail_route()
    def dataset(self, request, pk=None):
        myobject = self.get_object()
        serializer = self.get_serializer(myobject)
        return Response(serializer.data)


class KnxDataViewSet(viewsets.ModelViewSet):
    pagination_class = NoPagination
    queryset = models.KnxData.objects.all()
    serializer_class = serializers.KnxDataAllSerializer
    filter_class = KnxDataFilter


class FavChartViewSet(MultiSerializerViewSet):
    pagination_class = CustomPageNumberPagination
    queryset = models.FavChart.objects.all()
    serializers = {
        'default' : serializers.FavChartSerializer,
        'dataset': serializers.FavChartWithDataSerializer
    }

    @detail_route()
    def dataset(self, request, pk=None):
        myobject = self.get_object()
        serializer = self.get_serializer(myobject)
        return Response(serializer.data)

