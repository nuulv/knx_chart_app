(function ($) {
    moment.locale('pl');

    var app = $.sammy('#content-main', function () {
        this.disable_push_state = false;
        this.use('Template');

        this.get('#/', function (context) {
            var cur_page = (typeof this.params.page === 'undefined') ? 1 : this.params.page;
            var per_page = 100; // parm per_page in url can change default pagination size

            var chart_url = '/api/charts/?page=' + cur_page;

            context.log('charts url: ', chart_url);

            this.load('/static/templates/knxChart.template').replace(context.$element());

            $.getJSON(chart_url, function (data) {
                //context.log('results: ', data.results);

                context.renderEach('/static/templates/knxChartListItem.template', 'item', data.results)
                    .replace('#knxChartList tbody');

                var num_pages = Math.ceil(data.count / per_page);
                if (num_pages > 1) { // show pages buttons
                    var pages_list = paginate(cur_page, num_pages);

                    //$.each(pages_list, function (i, item) {
                    //    context.log(item.url, '-', item.text);
                    //});

                    context.renderEach('/static/templates/knxObjectListPages.template', 'page', pages_list)
                        .replace('.pagination');
                }
            })
                .fail(function (jqxhr, textStatus, error) {
                    context.render('/static/templates/knxObjectListError.template', {error: jqxhr.responseJSON})
                        .replace(context.$element());

                });
        });

        this.get('#/add', function (context) {
            context.render('/static/templates/knxChartAdd.template', {})
                .replace(context.$element())
                .then(function () {
                    // load select2
                    $('.js-example-responsive').select2({
                        language: "pl",
                        placeholder: "Wybierz obiekty",
                        multiple: true,
                        //tags: true,
                        tokenSeparators: [',', ' '],
                        //minimumResultsForSearch: 1,
                        minimumInputLength: 2,
                        ajax: {
                            url: '/api/objects/',
                            dataType: 'json',
                            type: 'GET',
                            delay: 250,
                            data: function (params) {
                                var queryParameters = {
                                    search: params.term,
                                    page: params.page,
                                    per_page: 10,
                                };
                                return queryParameters;
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    pagination: {
                                        more: (params.page * 10) < data.count
                                    },
                                    results: data.results
                                };
                            },
                            cache: true,
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        },
                        templateResult: function (results) {
                            var markup = "<div class='select2-result-repository clearfix'>" +
                                "<div><h4>" + results.name + "</h4></div>" +
                                "<div><strong>Obj id:</strong><i> " + results.obj_id +
                                "</i> <strong>Type:</strong><i> " + results.type + "</i></div></div>";
                            return markup;
                        },
                        templateSelection: function (results) {
                            return results.name;
                        }
                    });

                    var start = moment().subtract(29, 'days');
                    var end = moment();

                    $('#date-range').daterangepicker({
                        showDropdowns: true,
                        autoUpdateInput: false,
                        startDate: start,
                        endDate: end,
                        ranges: {
                            'Dzisiaj': [moment(), moment()],
                            'Wczoraj': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Ostatnie 7 dni': [moment().subtract(6, 'days'), moment()],
                            'Ostatnie 30 dni': [moment().subtract(29, 'days'), moment()],
                            'Ten miesiąc': [moment().startOf('month'), moment().endOf('month')],
                            'Ostatni miesiąc': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },

                        locale: {
                            format: "LL",
                            separator: " - ",
                            applyLabel: "Potwierdź",
                            cancelLabel: "Anuluj",
                            fromLabel: "Od",
                            toLabel: "Do",
                            customRangeLabel: "Własny zakres",
                            firstDay: 1
                        }
                    });

                });
        });

        this.post('#/add', function (context) {
            var dates = this.params['daterange'].split('-');
            var form_data = {
                name: this.params['chartname'],
                obj_ids: (this.params['objects'] instanceof Array) ? this.params['objects'] : [this.params['objects']],
                in_menu: (this.params['option'] === undefined) ? false : true
            }

            if (dates.length >= 2) {
                form_data.start_date = moment(dates[0], 'LL').format();
                form_data.stop_date = moment(dates[1], 'LL').format();
            }

            var tApp = this.app;
            $.ajax({
                async: false,
                url: '/api/charts/',
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                crossDomain: false,
                data: JSON.stringify(form_data),
                success: function (result) {
                    //tApp.refresh(); // TODO notification
                    $('#notifications').append(
                        '<div class="alert alert-success alert-dismissable"> ' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                        '<strong>Success!</strong> Udało się dodać wykres do bazy. ' +
                        '</div>'
                    );
                }
            })
                .fail(function () {}); // TODO
        });


        this.get('#/show/:id', function (context) {
            var chart_id = this.params['id'];
            context.load('/static/templates/knxChartShow.template').replace(context.$element())
                .then(function () {
                    var $target = $('#chart');

                    var getObjects = $.getJSON('/api/charts/' + chart_id + '/');

                    $.when(getObjects).done(function (response) {

                        var startDate = (response.start_date == null) ? '' : response.start_date;
                        var endDate = (response.stop_date == null) ? '' : response.stop_date;

                        var arrJqx = [];
                        response.obj_ids.map(function (obj_id) {
                            var dataUrl = '/api/data/?obj=' + obj_id +
                                '&date_s=' + startDate +
                                '&date_e=' + endDate;

                            arrJqx.push($.getJSON('/api/objects/' + obj_id + '/'));
                            arrJqx.push($.getJSON(dataUrl));
                        });

                        var current = 0;
                        var seriesOptions = [];
                        $.when.apply($, arrJqx).done(function () {
                            $.each(arguments, function (index, arg) {
                                var data = arg[0];
                                var chartData = [];
                                if (data instanceof Array) {
                                    $.map(data, function (item) {
                                        chartData.push([Date.parse(item.date), item.value]);
                                    });
                                    seriesOptions[current++].data = chartData;
                                } else {
                                    seriesOptions.push({name: data.name});
                                }
                            });
                        }).done(function () {
                            Highcharts.stockChart($target[0], {
                                chart: {
                                    //width: $chartRow.innerWidth(),
                                    //type: 'spline',
                                    zoomType: 'x'
                                },
                                plotOptions: {
                                    series: {
                                        showInNavigator: true,
                                        showInLegend: true,
                                        dataGrouping: {enabled: false}
                                    }
                                },
                                rangeSelector: {
                                    buttons: [{
                                        type: 'day',
                                        count: 3,
                                        text: '3d'
                                    }, {
                                        type: 'week',
                                        count: 1,
                                        text: '1w'
                                    }, {
                                        type: 'month',
                                        count: 1,
                                        text: '1m'
                                    }, {
                                        type: 'month',
                                        count: 6,
                                        text: '6m'
                                    }, {
                                        type: 'year',
                                        count: 1,
                                        text: '1y'
                                    }, {
                                        type: 'all',
                                        text: 'All'
                                    }],
                                    selected: 3
                                },
                                legend: {
                                    enabled: true,
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    borderWidth: 0
                                },
                                series: seriesOptions,

                            }).reflow();
                        });


                    });
                });
        });

        this.post('#/remove', function (context) {
            var tApp = this.app;
            var chart_id = this.params['id'];
            var chart_url = '/api/charts/' + chart_id;

            $.ajax({
                url: chart_url,
                cache: false,
                method: 'DELETE',
                success: function (result) {
                    tApp.refresh(); // TODO refresh only one row. result = edited row
                                    // TODO notification
                }
            }).fail(function () {}); // TODO error message
        })


    });

    $(function () {
        app.run('#/');

        // TODO make table and chart responsive
        $("#content-main").on('click', 'table', function (event) {
            event.stopPropagation();

            var $target = $(event.target);
            if (!($target.closest('td').attr('colspan') > 1)) {
                // check for chart if not present draw one

                var $chartRow = $target.closest('tr').next();
                if (!($chartRow.hasClass('drawn'))) {

                    $chartRow.find('div.loading').show();

                    //$.ajaxSetup({async: false});
                    var $targetData = $target.closest('tr');

                    var startDate = ($targetData.data('start') == null) ?
                        '' : moment($targetData.data('start')).format('YYYY-MM-DD');
                    var endDate = ($targetData.data('end') == null) ?
                        '' : moment($targetData.data('end')).format('YYYY-MM-DD');


                    var arrJqx = [];
                    $targetData.data('obj').map(function (obj_id) {
                        var dataUrl = '/api/data/?obj=' + obj_id +
                            '&date_s=' + startDate +
                            '&date_e=' + endDate;

                        arrJqx.push($.getJSON('/api/objects/' + obj_id + '/'));
                        arrJqx.push($.getJSON(dataUrl));
                    });


                    var seriesOptions = [];
                    var current = 0;
                    $.when.apply($, arrJqx).done(function () {
                        $.each(arguments, function (index, arg) {
                            var data = arg[0];
                            var chartData = [];
                            if (data instanceof Array) {
                                $.map(data, function (item) {
                                    chartData.push([Date.parse(item.date), item.value]);
                                });
                                seriesOptions[current++].data = chartData;
                            } else {
                                seriesOptions.push({name: data.name});
                            }
                        });

                    })
                        .done(function () {
                            Highcharts.stockChart($chartRow.find('div.chart').get(0), {
                                chart: {
                                    //width: $chartRow.innerWidth(),
                                    //type: 'spline',
                                    events: {
                                        load: function () {
                                            $chartRow.find('div.loading').hide();
                                        }
                                    },
                                    zoomType: 'x'
                                },
                                plotOptions: {
                                    series: {
                                        showInNavigator: true,
                                        showInLegend: true,
                                        dataGrouping: {enabled: false}
                                    }
                                },
                                rangeSelector: {
                                    buttons: [{
                                        type: 'day',
                                        count: 3,
                                        text: '3d'
                                    }, {
                                        type: 'week',
                                        count: 1,
                                        text: '1w'
                                    }, {
                                        type: 'month',
                                        count: 1,
                                        text: '1m'
                                    }, {
                                        type: 'month',
                                        count: 6,
                                        text: '6m'
                                    }, {
                                        type: 'year',
                                        count: 1,
                                        text: '1y'
                                    }, {
                                        type: 'all',
                                        text: 'All'
                                    }],
                                    selected: 3
                                },
                                legend: {
                                    enabled: true,
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    borderWidth: 0
                                },
                                series: seriesOptions,

                            });

                            $chartRow.addClass('drawn');
                            $target.closest('tr').next().find('div.chart').stop().slideToggle({
                                duration: 400,
                                step: function () {
                                    $chartRow.find('div.chart').highcharts().reflow();
                                }
                            });
                        });
                } else {
                    $target.closest('tr').next().find('div.chart').stop().slideToggle();
                }
            }
        });

        $('#content-main').on('click', '#preview', function (event) {
            event.preventDefault();
            if ($('#objects').val().length != 0) {
                var $chartTarget = $('#preview-area');
                $chartTarget.find('.loading').show();

                var dates = $('#date-range').val().split('-');
                var startDate = '';
                var endDate = '';

                if (dates.length >= 2) {
                    startDate = moment(dates[0], 'LL').format();
                    endDate = moment(dates[1], 'LL').format();
                }


                var arrJqx = [];
                $('#objects').val().map(function (obj_id) {
                    var dataUrl = '/api/data/?obj=' + obj_id +
                        '&date_s=' + startDate +
                        '&date_e=' + endDate;

                    arrJqx.push($.getJSON('/api/objects/' + obj_id + '/'));
                    arrJqx.push($.getJSON(dataUrl));
                });


                var seriesOptions = [];
                var current = 0;
                $.when.apply($, arrJqx).done(function () {
                    $.each(arguments, function (index, arg) {
                        var data = arg[0];
                        var chartData = [];
                        if (data instanceof Array) {
                            $.map(data, function (item) {
                                chartData.push([Date.parse(item.date), item.value]);
                            });
                            seriesOptions[current++].data = chartData;
                        } else {
                            seriesOptions.push({name: data.name});
                        }
                    });

                })
                    .done(function () {
                        Highcharts.stockChart($chartTarget.find('.chart').get(0), {
                            chart: {
                                //width: $chartRow.innerWidth(),
                                //type: 'spline',
                                events: {
                                    load: function () {
                                        $chartTarget.find('.loading').hide();
                                        $chartTarget.find('.chart').show();
                                    }
                                },
                                zoomType: 'x'
                            },
                            plotOptions: {
                                series: {
                                    showInNavigator: true,
                                    showInLegend: true,
                                    dataGrouping: {enabled: false}
                                }
                            },
                            rangeSelector: {
                                buttons: [{
                                    type: 'day',
                                    count: 3,
                                    text: '3d'
                                }, {
                                    type: 'week',
                                    count: 1,
                                    text: '1w'
                                }, {
                                    type: 'month',
                                    count: 1,
                                    text: '1m'
                                }, {
                                    type: 'month',
                                    count: 6,
                                    text: '6m'
                                }, {
                                    type: 'year',
                                    count: 1,
                                    text: '1y'
                                }, {
                                    type: 'all',
                                    text: 'All'
                                }],
                                selected: 3
                            },
                            legend: {
                                enabled: true,
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom',
                                borderWidth: 0
                            },
                            series: seriesOptions,

                        }).reflow();
                    });
            }
        });

        $('#content-main').on('click', '.remove', function (event) {
            event.preventDefault();
            event.stopPropagation();

            var $link = $(this);
            $("#modalConfirm").modal({backdrop: false, keyboard: false})
                .one('click', '#delete', function (event) {
                    app.runRoute('post', '#/remove', {id: $link.data('id')});
                });
        });

        $('#content-main').on('apply.daterangepicker', '#date-range', function (ev, picker) {
            $(this).val(picker.startDate.format('LL') + ' - ' + picker.endDate.format('LL'));
        });

        $('#content-main').on('cancel.daterangepicker', '#date-range', function (ev, picker) {
            $(this).val('');
        });
    });
})(jQuery);