/**
 * Created by nuulv on 2017-01-21.
 */

/*
if(pages > 9)
<< 1 2 3 4 .. 60 >>
   ^
<< 1 2 3 4 5 .. 60 >>
     ^
<< 1 2 3 4 5 6 .. 60 >>
       ^
<< 1 2 3 4 5 6 7 .. 60 >>
         ^
<< 1 2 3 4 5 6 7 8 .. 60 >>
           ^
<< 1 .. 3 4 5 6 7 8 9 .. 60 >>
              ^
<< 1 .. 4 5 6 7 8 9 10 .. 60 >>
              ^
<< 1 .. 53 54 55 56 57 58 59 60 >>
                    ^^
<< 1 .. 53 54 55 56 57 58 59 60 >>
                 ^^
<< 1 .. 53 54 55 56 57 58 .. 60 >>
              ^^
else
<< 1 2 3 4 5 6 7 8 9>>
*/
function paginate(cur_page, pages){

    var pages_list = [];

    if (pages > 1){

        pages_list.push({ // << prev page
            url: (cur_page - 1 >= 1)?cur_page-1:"",
            text: "\u00AB", // <<
            class: (cur_page == 1)? "disabled": "",
        });

        if ( pages > 9 ) {

            if (cur_page >= 5) {
                pages_list.push({
                    url: 1,
                    text: "1",
                    class: ""
                }); // push 1 page
            }
            if (cur_page >= 6) {
                pages_list.push({
                    url: "",
                    text: "..",
                    class: "disabled",
                });// push ...
            }


            // 5 pages before current
            var pages_before = (cur_page - 3 < 1) ? 1 : cur_page - 3;
            if (pages_before != 0) { // nie ma strony 0
                for (var i = pages_before; i <= cur_page - 1; i++) {
                    pages_list.push({
                        url: i,
                        text: i,
                        class: "",
                    });
                }
            }
            pages_list.push({
                url: cur_page,
                text: cur_page,
                class: "active",
            }); // current_page


            // 5 pages after current
            var pages_after = (cur_page + 3 > pages) ? pages : cur_page + 3;
            //if (pages_after == 0 ) {
            for (var i = cur_page + 1; i <= pages_after; i++) {
                pages_list.push({
                    url: i,
                    text: i,
                    class: "",
                });
            }
            //}
            if (cur_page <= pages - 5) {
                pages_list.push({
                    url: "",
                    text: "..",
                    class: "disabled",
                });// push ...
            }
            if (cur_page <= pages - 4) {
                pages_list.push({
                    url: pages,
                    text: pages,
                    class: ""
                }); // push last page
            }
        }else{
            for(var i = 1; i <= pages; i++){
                pages_list.push({
                    url: i,
                    text: i,
                    class: (i == cur_page) ? "active":"",
                });
            }
        }

        pages_list.push({ // next page
            url: (cur_page + 1 <= pages)?cur_page+1:"",
            text: "\u00BB", // >>
            class: (cur_page == pages)?"disabled":"",
        });



    }

    return pages_list;
}