from django.apps import AppConfig


class MyAppConfig(AppConfig):
    name = 'knxApp'
    verbose_name = "Knx Chart server"

    def ready(self):
        print('app started')


