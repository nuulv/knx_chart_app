from django.contrib import admin
from .models import *


class KnxObjectsAdmin(admin.ModelAdmin):
    list_display = ('obj_id', 'name', 'type', 'monitor')
    empty_value_display = '-empty-'


class KnxDataAdmin(admin.ModelAdmin):
    list_display = ('obj', 'date', 'value')

    def has_add_permission(self, request):
        return False


# class FavChartAdmin(admin.ModelAdmin):
#     list_display = ('name', 'start_date', 'stop_date', 'obj_ids', 'in_menu')
#     empty_value_display = '-empty-'

admin.site.register(KnxObjects, KnxObjectsAdmin)
admin.site.register(KnxData, KnxDataAdmin)
admin.site.register(FavChart)
# admin.site.register(FavChart, FavChartAdmin)

