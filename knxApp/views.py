from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils import six
from django.utils.text import capfirst
from django.utils.translation import ugettext_lazy as _
from django.views import View
from django.views.generic import TemplateView, ListView
from django.contrib import messages
from .mixin import *


class KnxDashboard(StandardMixin, TemplateView):
    template_name = 'knxDashboard/knxDashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({})
        return context

    # def get(self, request, *args, **kwargs):
    #    return super(KnxDashboard, self).get(request, *args, **kwargs)


class KnxObjectList(StandardMixin, ListView):
    template_name = 'knxDashboard/knxObjectList.html'
    model = KnxObjects

    #def post(self, request):
    #    return HttpResponse('post: %s'%request)


class KnxChart(StandardMixin, ListView):
    template_name = 'knxDashboard/knxChart.html'
    model = FavChart

    #def post(self, request):
    #    # TODO notification
    #    qs = FavChart.objects.all()

    #    list = map(int, request.POST.getlist('in_menu', None))

    #   for item in qs:
    #       print(item.id in list)
    #       if item.id in list:
    #           item.in_menu=True
    #       else:
    #           item.in_menu=False
    #        item.save()

    #    messages.info(request, 'Zapisano zmiany.')

        # return render(request, self.template_name, self.context)
    #    return HttpResponseRedirect('/charts/')

'''
class KnxSettings(StandardMixin, View):
    template_name = 'knxDashboard/knxSettings.html'

    def get(self, request, app_slug=None):
        # Determine what set of settings this editor is used for
        if app_slug is None:
            settings = loading.get_all_settings()
            title = _('Site settings')
        else:
            settings = loading.get_app_settings(app_slug)
            title = _('%(app)s settings') % {'app': capfirst(app_slug)}

        # Create an editor customized for the current user
        editor = forms.customized_editor(request.user, settings)

        form = editor()

        return render(request, self.template_name, {
            'title': title,
            'form': form,
            'user_charts': FavChart.objects.filter(in_menu=True),
        })

    def post(self, request, app=None):
        if app is None:
            settings = loading.get_all_settings()
        else:
            settings = loading.get_app_settings(app)

        editor = forms.customized_editor(request.user, settings)
        # Populate the form with user-submitted data
        form = editor(request.POST.copy(), request.FILES)
        if form.is_valid():
            form.full_clean()

            for name, value in form.cleaned_data.items():
                key = forms.RE_FIELD_NAME.match(name).groups()
                setting = loading.get_setting(*key)
                try:
                    storage = loading.get_setting_storage(*key)
                    current_value = setting.to_python(storage.value)
                except:
                    current_value = None

                if current_value != setting.to_python(value):
                    args = key + (value,)
                    loading.set_setting_value(*args)

                    # Give user feedback as to which settings were changed
                    if setting.class_name:
                        location = setting.class_name
                    else:
                        location = setting.module_name
                    update_msg = (_('Updated %(desc)s on %(location)s') %
                                  {'desc': six.text_type(setting.description),
                                   'location': location})
                    messages.add_message(request, messages.INFO, update_msg)

            return HttpResponseRedirect(request.path)


class KnxSetup(StandardMixin, TemplateView):
    template_name = 'knxDashboard/knxSetup.html'
'''
