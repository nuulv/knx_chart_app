from django.conf.urls import include
import macrosurl
from macrosurl import url
from django.contrib.auth import views as auth_view
from rest_framework import routers
from . import views as view
from . import rest_views as rest


# ^/(?P<pk>[^/.]+)\.(?P<format>[a-z0-9]+)/?$
# . => http://www.django-rest-framework.org/api-guide/format-suffixes/
router = routers.DefaultRouter()
router.register(r'objects',  rest.KnxObjectsViewSet)
router.register(r'data',     rest.KnxDataViewSet)
router.register(r'charts',   rest.FavChartViewSet)


macrosurl.register('num', '\d+')

urlpatterns = [
    # rest views
    url(r'^api/', include(router.urls)),

    # app urls
    url(r'^$',             view.KnxDashboard,  name='home',         kwargs={}),
    url(r'^objects/$',     view.KnxObjectList, name='objects',      kwargs={}),
    url(r'^charts/$',      view.KnxChart,      name='create_chart', kwargs={}),
    #url(r'^setup/$',       view.KnxSetup,      name='app_setup',    kwargs={}),
    #url(r'^favchart/(?P<id>[^/]+)/$', view.KnxBaseView.as_view()),

    # auth urls
    url(r'^login/$',   auth_view.login,  name='login',  kwargs={'template_name': 'core/login.html'}),
    url(r'^logout/$',  auth_view.logout, name='logout', kwargs={'next_page': '/'}),

    # appsettings url
    #url(r'^settings/$',                 view.KnxSettings, name='site_settings', kwargs={}),
    #url(r'^settings/:app_slug/$',       view.KnxSettings, name='app_settings',  kwargs={}),
]

